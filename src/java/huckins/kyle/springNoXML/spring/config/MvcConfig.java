
package huckins.kyle.springNoXML.spring.config;

import org.springframework.context.annotation.*;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.config.annotation.*;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

@EnableWebMvc   //Enables Spring WebMVC(<mvc:annotation-driven />)
@Configuration  //Defines this as a class that declares @Bean methods
//Scans for controllers(<context:component-scan base-package="huckins.kyle.springNoXML.controller" >)
@ComponentScan(basePackages = {"huckins.kyle.springNoXML.controller"}) 
public class MvcConfig extends WebMvcConfigurerAdapter {
    
    /**
     * addResourceHandlers
     * requires ResourceHandlerRegistry
     * 
     * Maps Resources folder and contents defined by .addResourceHandler to a 
     * location defined by .addResourceLocations.  Allows for dynamic loading 
     * of resources regardless of location.
     * Equivalent to <mvc:resources mapping="/resources/**" location="/resources/"/>
     * @param registry 
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/resources/**").addResourceLocations("/resources/");
    }
    
    /**
     * configureDefaultServletHandling
     * requires DefaultServletHandlerConfigurer
     * 
     * Enables configuring a request handler for serving static resources by forwarding the 
     * request to the Servlet container's "default" Servlet. This is intended to be used when 
     * the Spring MVC DispatcherServlet is mapped to "/" thus overriding the Servlet container's 
     * default handling of static resources. Since this handler is configured at the lowest 
     * precedence, effectively it allows all other handler mappings to handle the request, 
     * and if none of them do, this handler can forward it to the "default" Servlet.
     * @param configurer 
     */
    @Override
    public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
        configurer.enable();
    }
    
    /**
     * jspViewResolver
     * 
     * Resolves model so that it can be rendered in a browser without 
     * being tied to a particular technology.  In this ViewResolver,
     * the prefix of a view is set to /WEB-INF/views/, suffixed by 
     * .jsp.  
     * With this you can refer to a jsp by its name.  When a controller
     * returns "helloworld," the ViewResolver will prefix and suffix it
     * with the defined strings.
     * Equivalent of:
     * <bean id="viewResolver"
     *    class="org.springframework.web.servlet.view.InternalResourceViewResolver"
     *    p:prefix="/WEB-INF/jsp/"
     *    p:suffix=".jsp" />
     * @return 
     */
    @Bean
    public InternalResourceViewResolver jspViewResolver() {
        InternalResourceViewResolver bean = new InternalResourceViewResolver();
        bean.setPrefix("/WEB-INF/views/");
        bean.setSuffix(".jsp");
        return bean;
    }
    
    /**
     * getMultipartResolver
     * 
     * gets a new CommonsMultipartResolver,  a common 
     * MultipartResolver implementation, which use the 
     * Apache commons upload library to handle the file 
     * upload in a form.  CommonsMultipartResolver Provides 
     * maxUploadSize, maxInMemorySize, and defaultEncoding 
     * settings as bean properties; see respective 
     * DiskFileUpload properties (sizeMax, sizeThreshold, 
     * headerEncoding) for details in terms of defaults and 
     * accepted values.  Saves temporary files to the servlet 
     * container's temporary directory. Needs to be initialized 
     * either by an application context or via the constructor 
     * that takes a ServletContext (for standalone usage).
     * 
     * @return 
     */
    @Bean(name = "multipartResolver")
    public CommonsMultipartResolver getMultipartResolver() {
        return new CommonsMultipartResolver();
    }
    /**
     * getMessageSource
     * 
     * Creates and returns ReloadableResourceBundleMessageSource, 
     * resource, and sets its Basename as classpath:messages.  
     * Encoding is UTF-8.
     * Spring-specific MessageSource implementation that accesses 
     * resource bundles using specified basenames, participating 
     * in the Spring ApplicationContext's resource loading.
     * In contrast to the JDK-based ResourceBundleMessageSource, 
     * this class uses Properties instances as its custom data 
     * structure for messages, loading them via a PropertiesPersister 
     * strategy from Spring Resource handles. This strategy is not 
     * only capable of reloading files based on timestamp changes, 
     * but also of loading properties files with a specific character 
     * encoding. It will detect XML property files as well.
     * More info at: http://docs.spring.io/spring/docs/current/javadoc-api/org/springframework/context/support/ReloadableResourceBundleMessageSource.html
     * @return 
     */
    @Bean(name = "messageSource")
    public ReloadableResourceBundleMessageSource getMessageSource() {
        ReloadableResourceBundleMessageSource resource = new ReloadableResourceBundleMessageSource();
        resource.setBasename("classpath:messages");
        resource.setDefaultEncoding("UTF-8");
        return resource;
    }
 
}
